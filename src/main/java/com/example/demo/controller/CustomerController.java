package com.example.demo.controller;

import com.example.demo.model.Customer;
import com.example.demo.model.LoginRequest;
import com.example.demo.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/customers")
@AllArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/login")
    public Customer login(@RequestBody LoginRequest loginRequest) {
        return customerService.login(loginRequest);
    }
}
