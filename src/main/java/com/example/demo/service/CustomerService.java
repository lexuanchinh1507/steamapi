package com.example.demo.service;

import com.example.demo.model.Customer;
import com.example.demo.model.LoginRequest;
import com.example.demo.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public Customer login(LoginRequest loginRequest) {
        return customerRepository.findFirstByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword())
                .orElseThrow(() -> new RuntimeException("username hoặc mật khẩu không chính xác"));
    }
}
