package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "PHIEUDATHANG")
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MaPhieuDH", nullable = false)
    private Long id;

    @Column(name = "MaKH", nullable = false)
    private Long customerId;

    @Column(name = "NgayDat", nullable = false)
    private LocalDateTime orderDate;

    @Column(name = "Tong_SL_Dat", nullable = false)
    private Integer quantity;

    @Column(name = "ThanhTien", nullable = false)
    private Float total;

    @Column(name = "TinhTrang", nullable = false)
    private Integer status;

    @Column(name = "PhiShip", nullable = false)
    private Float shipFee;

}