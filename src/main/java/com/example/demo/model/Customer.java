package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "KHACHHANG")
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MaKH", nullable = false)
    private Long id;

    @Column(name = "TenKH", nullable = false)
    private String name;

    @Column(name = "DiaChi", nullable = false)
    private String address;

    @Column(name = "SDT", nullable = false)
    private String phoneNumber;

    @Column(name = "Email", nullable = false)
    private String email;

    @Column(name = "TenDN", nullable = false)
    private String username;

    @Column(name = "MatKhau", nullable = false)
    private String password;

}
